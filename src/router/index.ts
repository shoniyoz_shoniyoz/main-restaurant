import {createRouter, createWebHistory} from "vue-router";

function authGuard(to: any, from: any, next: any) {
  if (localStorage.getItem("token")) {
    next();
  } else {
    next("/login");
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      meta: {layout: "default"},
      component: () => import("../pages/login/Login.vue"),
    },
    {
      path: "/",
      name: "home",
      meta: {layout: "main"},
      component: () => import("../pages/home/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/delivery",
      name: "delivery",
      meta: {layout: "main"},
      component: () => import("../pages/delivery/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/booking",
      name: "booking",
      meta: {layout: "main"},
      component: () => import("../pages/booking/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/restaurant",
      name: "restaurant",
      meta: {layout: "main"},
      component: () => import("../pages/restaurant/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/clients",
      name: "clients",
      meta: {layout: "main"},
      component: () => import("../pages/clients/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/category-restaurant",
      name: "category-restaurant",
      meta: {layout: "main"},
      component: () => import("../pages/categoryRetaurant/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/popular-dishes",
      name: "popular-dishes",
      meta: {layout: "main"},
      component: () => import("../pages/popularDishes/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/rooms",
      name: "rooms",
      meta: {layout: "main"},
      component: () => import("../pages/rooms/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/staff",
      name: "staff",
      meta: {layout: "main"},
      component: () => import("../pages/staff/Index.vue"),
      beforeEnter: authGuard
    },
    {
      path: "/admin",
      name: "admin",
      meta: {layout: "main"},
      component: () => import("../pages/admin/Index.vue"),
      beforeEnter: authGuard
    },
  ],
});


export default router;
