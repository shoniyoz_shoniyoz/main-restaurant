import {defineStore} from "pinia";
import axios from "axios";

const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_IMAGE
});

export const useImagesStore = defineStore("image", {
  state: () => ({
    images: [],
    image: null,
  }),
  actions: {
    async postImages(form: FormData) {
      try {
        this.images = await $axios.post('/files', form)
      } catch (error) {
        return error
      }
    },
    async postImage(form: FormData) {
      try {
        this.image = await $axios.post('/file', form)
      } catch (error) {
        return error
      }
    },
  },
});
