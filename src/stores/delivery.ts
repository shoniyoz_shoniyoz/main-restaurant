import {defineStore} from "pinia";
import $axios from "../plugins/axios";

export const useDelivery = defineStore("delivery", {
  state: () => ({
    allData: [],
    openData: [],
    acceptedDispatcherData: [],
    acceptedRestaurantData: [],
    closeData: [],
    oneTheWayData: [],
    readyData: [],
    rejectedData: []
  }),
  actions: {
    fetchStatusDelivery: async function () {
      return new Promise((resolve, reject) => {

        const day = getDay()
        console.log(day)
        $axios.get('/delivery')
          .then(({data}) => {
            this.allData = data.deliveries
            this.openData = data.deliveries.filter((a: any) => a.status === 'open')
            this.acceptedDispatcherData = data.deliveries.filter((a: any) => a.status === 'accepted' && a.by === 'restaurant')
            this.acceptedRestaurantData = data.deliveries.filter((a: any) => a.status === 'accepted' && a.by === 'admin')
            this.closeData = data.deliveries.filter((a: any) => a.status === 'close' && day === a.day)
            this.oneTheWayData = data.deliveries.filter((a: any) => a.status === 'on_the_way')
            this.readyData = data.deliveries.filter((a: any) => a.status === 'ready')
            this.rejectedData = data.deliveries.filter((a: any) => a.status === 'rejected' && day === a.day)
            resolve(data)
          })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },
});

function getDay() {
  const date = new Date()
  const getDate = date.getDate()
  const day = getDate > 9 ? `${getDate}` : `0${getDate}`
  const getMonth = date.getMonth() + 1
  const month = getMonth > 9 ? `${getMonth}` : `0${getMonth}`

  return `${day}-${month}`
}
