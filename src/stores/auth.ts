import {defineStore} from "pinia";
// @ts-ignore
import $axios from "@/plugins/axios";
// @ts-ignore
import {removeItem, setItem} from "../helpers/persistaneStorage"


export const useAuth = defineStore("auth", {
  state: () => ({
    user: null,
  }),
  actions: {
    login(user: any) {
      return new Promise((resolve, reject) => {
        $axios.post('/auth/login', user)
          .then((respose: any) => {
            resolve(respose)
            removeItem('token')
            setItem("token", respose.data.token)
            $axios.defaults.headers.common.authorization = `Bearer ${respose.data.token}`;
          })
          .catch((error: any) => {
            reject(error)
          })
      })

    },
    logout() {
      removeItem('token')
    },
    fetchDelivery() {
      return new Promise((resolve, reject) => {
        $axios.get('/delivery').then((response) => {
          resolve(response)
          console.log(response, 'res')
        })
          .catch((error) => {
            reject(error)
          })
      })
    }
  },
});
