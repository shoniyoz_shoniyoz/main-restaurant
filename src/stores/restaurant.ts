import {defineStore} from "pinia";
import $axios from "@/plugins/axios";

export const useRestaurantStore = defineStore("restaurant", {
  state: () => ({
    restaurantType: [],
    restaurantAll: []
  }),
  actions: {
    async fetchRestaurantType() {
      try {
        this.restaurantType = await $axios.get('/type/')
      } catch (error) {
        return error
      }
    },
    async putRestaurant(form: Object) {
      try {
        await $axios.put('', form)
      } catch (e) {
        return e
      }
    },
    async fetchRestaurant() {
      try {
        this.restaurantAll = await $axios.get('/')
      } catch (error) {
        return error
      }
    },
    async deleteRestaurant(id: number) {
      try {
        await $axios.delete(`/restaurant/${id}`)
      } catch (error) {
        return error
      }
    }
  },
});

