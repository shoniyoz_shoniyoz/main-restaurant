import axios from "axios";
const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
  },
});
$axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.data.error) error.message = error.response.data.error;
    return Promise.reject(error);
  }
);

export default $axios;


export const getToken: () => void = () => {
  const token: string | null = localStorage.getItem("token");

  if (token) {
    $axios.defaults.headers.common.authorization = `Bearer ${token}`;
  }
};
getToken()
